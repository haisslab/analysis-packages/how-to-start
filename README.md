# How to start

## What is a developpement environement

Python is an open source software. Desipe it's trendyness, Python is quite old (already more tahn 30 years old) and have been one of the most used languages in the world for at least 15 years. 
Taken together, this means that a lot of open source *packages*, i.e. code written by some persons and distributed to allow others to use it, are existing in different versions. Some are regularly updated, and have a good compatibility between versions, some have evolved a lot and past versions are not compatible with them at all anymore, and some have stopped being maintained (updated and bugfixed) and will forever remain available only with a specific set of other packages and python versions. 
So to deal with this mess of versions, (that is not specific to python) the concept of developpement environment was born. It means that on a given machine, you can install several instances of *Python* that will have attached a bunch of packages that should be compatible with it.  
You can install packages in **developpement environement A with python 2.7** and **environment B with python 3.8** for example. Both will be able to execute on your computer, and even at the same time. Because you will have two copies of the Python language on your machine, and have installed in **environment A** a selection of packages that are compatible with python 2.7, and in *B*, with python 3.8.

A software that allows to perform such environment manipulation on windows and that is very commonly used, is **Anaconda**, or conda for short.

## First install Anaconda

- Download anaconda for windows [here](https://www.anaconda.com/products/distribution)  
- Run the installation file
- Open a command prompt terminal by typing `Command Prompt` in the windows searchbar
- Type `conda --help` :
  - If you got printed out on screen the conda help, perfect, jump to the next step
  - Otherwise, type `environment variables` in windows searchbar, and then click on `Edit the environment variables`
  - In the window that opened up, press on the button `Environment Variables` 
  - In the bottom field, search for the line called `Path`. Highlight it in blue and press the button `Edit`
  - In the window that opened up, click the button `New` and paste : `C:\ProgramData\Anaconda3\condabin` (This should be the default path o the `conda.bat` file)
  - That should do the trick. Try closing the command prompt, eventuallly reboot the computer and test again the command `conda --help`

## Create your developement environment

Create a developpement environment named `Analysis`, for example.

- To do so, in a terminal, (Open a command prompt terminal by typing `Command Prompt` in the windows searchbar) type : 
  ```
  conda create -n Analysis python=3.12
  ```
  This will create an environment with python installed in version 3.12.
  Replace Analysis with the name of your enviroment if you want to create it with another name.

## **Activate** your developement environment
After creation of your developement environment, to install some packages, you need to activate your environment.
To **activate** it, you can run in your console : 

```
conda activate Analysis
```
You can of course replace Analysis with the name of any of your enviroments. If you forgot their names, you can see all of them by typing :
`conda env list`

If the console claims that it needs to be initialized, type : `conda init`. Then close the console, open a new one and redo the step above. This init command will never need to be performed again.

## Install packages
Packages are sometimes also called libraries, and are code you want to install to be able to use.
Numpy, Pandas, Matplotlib are 3 example packages that are used all the time during analysis.

Packages can be writen so that on install, they also trigger the auto-resolution of the packages that they depend on, on the most recent version that can
satisfy all the dependencies of all packages recursively.

So, best case scenario, installing the required code for running analysis in your prefered IDE (most probably jupyter lab or jupyter notebook), 
you can just do : 

```
pip install git+https://gitlab.pasteur.fr/haisslab/analysis-packages/researchprojects.git
```

This will allow you to import the ``ResearchProjects`` package from python, wich will be plenty usefull for your analysis.

This package contains analysis and protting functions that will have been developped for specific projects in the lab. They may not be applicable for all projects so they will not be included in Inflow. Each project set of function in ResearchProjects have it's own subpackage  
In this lab, for analysis, we have 3 main packages that are developped to help ou do your analysis and produce / share code / data with others in the lab. They are described in the section below.

If however you do not only want to use it but to make changes to it (add your needs and developp it for your ResearchProject in the lab) then you may run instead : (can be done after the previous command just fine)

```
cd C:/Users/<me>/Documents/Python/Sources
git clone https://gitlab.pasteur.fr/haisslab/analysis-packages/researchprojects.git
cd ResearchProjects
pip install -e .
```

Please just replace \<me\> with your username. 

## Packages that will be auto installed (dependancies)

The 3 main packages for analysis as described above, are listed below.

In case you also want to edit them, :
- Open a terminal
- activate your environment (see above in anaconda **activate** section)
- clone them in your python sources folder (C:/Users/``<me>``/Documents/Python/Sources)
- go into the cloned folder (cd <my_cloned_repo_folder>)
- and install them with ``pip install -e .``

- ### [Inflow](https://gitlab.pasteur.fr/haisslab/analysis-packages/Inflow)
  ```
  git clone https://gitlab.pasteur.fr/haisslab/analysis-packages/Inflow.git
  ```  
  This is the package containing the core utilities for reading data files from the setups in the lab, creating pipelineable functions etc, and a bunch of utilitiaries and helpers as well as some specific plot tools.  
  (Installing this package will install suite2p automatically)  

- ### [ResearchProjects](https://gitlab.pasteur.fr/haisslab/analysis-packages/researchprojects)
  ```
  git clone https://gitlab.pasteur.fr/haisslab/analysis-packages/researchprojects.git
  ```  
  This package contains analysis and protting functions that will have been developped for specific projects in the lab. They may not be applicable for all projects so they will not be included in Inflow. Each project set of function in ResearchProjects have it's own subpackage  

- ### [Alyx Connector](https://gitlab.pasteur.fr/haisslab/data-management/ONE)
  ```
  git clone https://gitlab.pasteur.fr/haisslab/data-management/ONE.git
  ```    
  This package is the one that allows to dialogue between Alyx, the database that contains the metadatas of any experiment done in the lab, and us the users. It allows to search sessions, set quality checks, and download raw data or processed data files from/to the file storage servers.

## Using a code editor

IDE stands for Integrated Development Environment. It refers to any kind of graphical user interface to write code and execute it. A good IDE also features a way to debug your code by showing and allowing you to trace back errors (at compilation for C or compiled languages, and at runtime, for any language including scripting languages like Python), set breakpoints (ask to pause your code when it reaches a given code line, allowing you to see the guts of the code at that specific location), perform profiling (measure how your code runs in time, and which lines or parts of it are taking the most time to run, to help you increase it's performance)... 

**That said, there is several types of IDEs for scripting languages like python.** :  
IDEs that integrate `cells`, and other ones.

### IDEs with cells (for analysis):

As a scripting language, you can run parts of your code and write some new code on the fly to use the ouptuts of the previous lines of code that you just executed. Not everything needs to be written in advance like for compiled languages. This is a particularly usefull feature to interleave code with comments in rich text (for example formatted with latex or markdown) and figures/plots. It also allows to dynamically use variables and code outputs and ineract with them more directly. Jupyter is an API that supports python, built on top of IPython (interactive python) and cells editors were basically invented with Jupyter. They are hence called Jupyter notebooks, and their file extension is .ipynb for ipython notebooks. Notebooks are cells that are ordered inside a document.

Good IDEs for Jupyter are :

- **jupyter lab**
  Install it **inside your conda virtual environment** by typing `conda install -c conda-forge jupyterlab`. Then run it by calling `jupyter lab` in your environment.
- jupyter notebook (older)

Some other IDE also support cells :
- Visual Studio Code (less easy to work with than the two above)
- Spyder (but do not allow to open jupyter notebooks and instead rely on "cells like" structures. Prefer to not use this one for analysis)

### IDEs without cells (for packages):
But cells are not usefull to write packages, wich are collections of tools (classes/functions) that can be used by other users later on.
This is usually something a bit more advanced than just writing code for analysis and requires some knowledge on how import works and how packaging is done.

Undeniably, the best IDE for python is VSCode (it's also the best ide for any language in fact...) :

- **Visual Studio Code** (developped by microsoft. Free. A bit more tricky to use)

In the past, spyder was cool for python, as it can be installed from command line. But compated to VSCode, it pales out greatly and is now completely outdated.

- **Spyder** (open source, Free, Simple to use and install)
  Install it **inside your conda virtual environment** by typing : `conda install -c anaconda spyder`. Then run by calling `spyder` in your environment.

## Trying out to run some code

Once packages and an IDE have been installed, you can try out some code by looking at some jupyter notebook examples from the lab here : [notebook_tutorials](https://gitlab.pasteur.fr/haisslab/projects_analyses/notebook_tutorials)